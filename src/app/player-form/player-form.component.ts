import {Component, DoCheck, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {PlayerService} from '../services/PlayerService';
import {NgForm} from '@angular/forms';
import {Player} from '../models/Player';

enum FormMode {
    Normal = 'NORMAL',
    Editing = 'EDITING'
}

@Component({
    selector: 'app-player-form',
    templateUrl: './player-form.component.html',
    styleUrls: ['./player-form.component.scss']
})
export class PlayerFormComponent implements OnInit, DoCheck, OnDestroy {
    @ViewChild('form') form: NgForm;
    playerPositions: string[];
    isVisible: boolean;
    formMode = FormMode.Normal;
    formTitle = 'Kreiraj igrača';
    
    private defaultModelPlayer = {
        'id': '',
        'firstName': '',
        'lastName': '',
        'shirtNumber': '',
        'position': '',
    };
    modelPlayer = this.defaultModelPlayer;
    
    constructor(private playerService: PlayerService) {}
    
    ngOnInit() {
        this.isVisible = this.playerService.getFormIsVisible();
        this.playerPositions = this.playerService.getPlayerPositions();
        
        this.playerService.formSubject.subscribe((player: Player) => {
            this.modelPlayer = Object.assign({}, this.modelPlayer, player);
            this.playerService.setFormVisibility(true);
            this.formMode = FormMode.Editing;
        });
    }
    
    ngDoCheck() {
        this.isVisible = this.playerService.getFormIsVisible();
        
        if (this.formMode === FormMode.Editing) {
            this.formTitle = 'Uredi igrača';
        } else {
            this.formTitle = 'Kreiraj igrača';
        }
    }
    
    ngOnDestroy() {
        this.playerService.formSubject.unsubscribe();
    }
    
    onSubmit() {
        
        if (this.form.valid && this.form.value.position !== '') {
            
            if (this.formMode === FormMode.Editing) {
                this.playerService.updatePlayer(
                    this.modelPlayer.id,
                    Object.assign({}, this.modelPlayer, {
                        'firstName': this.form.value.firstName,
                        'lastName': this.form.value.lastName,
                        'shirtNumber': parseInt(this.form.value.shirtNumber, 10),
                        'position': this.form.value.position
                    })
                );
            } else {
                this.playerService.createPlayer(
                    this.form.value.firstName,
                    this.form.value.lastName,
                    parseInt(this.form.value.shirtNumber, 10),
                    this.form.value.position
                );
            }
            
            this.form.reset(this.defaultModelPlayer);
            this.closeForm();
            
        } else if (!this.form.valid || this.form.value.position === '') {
            
            this.form.controls.firstName.markAsTouched();
            this.form.controls.lastName.markAsTouched();
            this.form.controls.shirtNumber.markAsTouched();
            this.form.controls.position.markAsTouched();
            
        }
        
    }
    
    closeForm() {
        this.formMode = FormMode.Normal;
        this.modelPlayer = this.defaultModelPlayer;
        this.playerService.setFormVisibility(false);
    }
}
