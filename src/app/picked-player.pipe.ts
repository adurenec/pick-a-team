import {Pipe, PipeTransform} from '@angular/core';
import {Player} from './models/Player';

@Pipe({
    name: 'pickedPlayer'
})
export class PickedPlayerPipe implements PipeTransform {
    
    transform(players: Player[], isPicked: boolean): Player[] {
        return players.filter(p => p.isPicked === isPicked);
    }
    
}
