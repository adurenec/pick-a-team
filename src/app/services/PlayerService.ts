import {Subject} from 'rxjs/Subject';
import {Player} from '../models/Player';
import playerData from '../mock-data/player-data.json';

import guid from '../helpers/guid';

export class PlayerService {
    selectPlayer = new Subject();
    formSubject = new Subject();
    showForm = false;
    playerMap: Map<string, Player> = new Map();
    selectedPlayerID: string;
    playerPositions = [
        'Golman',
        'Lijevi bek',
        'Desni bek',
        'Lijevi vezni',
        'Desni vezni',
        'Napadač'
    ];
    
    constructor() {
        for (const player of playerData) {
            this.playerMap.set(player.id, new Player(
                player.id, player.firstName, player.lastName, player.shirtNumber, player.position, player.isPicked
            ));
        }
    }
    
    getPlayerPositions() {
        return this.playerPositions;
    }
    
    getPlayers() {
        return Array.from(this.playerMap.values());
    }
    
    private addPlayer(newPlayer: Player) {
        this.playerMap.set(newPlayer.id, newPlayer);
    }
    
    createPlayer(firstName, lastName, shirtNumber, position) {
        const newId = guid();
        
        this.addPlayer(new Player(
            newId, firstName, lastName, shirtNumber, position, false
        ));
    }
    
    deletePlayer(id: string) {
        this.playerMap.delete(id);
    }
    
    updatePlayer(id: string, newPlayerModel) {
        const currentPlayer = this.playerMap.get(id);
        this.playerMap.set(id, Object.assign({}, currentPlayer, newPlayerModel));
    }
    
    transferSelected() {
        const id = this.selectedPlayerID;
        
        if (!id) {
            return;
        }
        
        const currentPlayer = this.playerMap.get(id);
        this.playerMap.set(id, Object.assign({}, currentPlayer, { isPicked: !currentPlayer.isPicked }));
    }
    
    setPlayerSelected(id: string) {
        if (this.selectedPlayerID === id) {
            this.selectedPlayerID = '';
        } else {
            this.selectedPlayerID = id;
        }
    }
    
    getSelectedPlayer() {
        const id = this.selectedPlayerID;
        
        if (!id) {
            return;
        }
        
        const player = this.playerMap.get(id);
        
        if (player) {
            return player;
        }
        
        return null;
    }
    
    setFormVisibility(shouldShow: boolean) {
        this.showForm = shouldShow;
    }
    
    getFormIsVisible() {
        return this.showForm;
    }
}

