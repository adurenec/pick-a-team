import {Component, DoCheck, OnDestroy, OnInit} from '@angular/core';
import {PlayerService} from '../services/PlayerService';
import {Player} from '../models/Player';

enum PlayerSide {
    Left = 'LEFT',
    Right = 'RIGHT'
}

@Component({
    selector: 'app-player-picker',
    templateUrl: './player-picker.component.html',
    styleUrls: ['./player-picker.component.css']
})
export class PlayerPickerComponent implements OnInit, DoCheck, OnDestroy {
    players: Player[];
    selectedID: string;
    selectedPlayer: Player;
    
    constructor(private playerService: PlayerService) {
    }
    
    ngOnInit() {
        this.players = this.playerService.getPlayers();
        
        this.playerService.selectPlayer.subscribe((id: string) => {
            this.playerService.setPlayerSelected(id);
            this.selectedID = this.playerService.selectedPlayerID;
        });
    }
    
    ngDoCheck() {
        this.players = this.playerService.getPlayers();
    }
    
    ngOnDestroy() {
        this.playerService.selectPlayer.unsubscribe();
    }
    
    onArrowClick() {
        this.playerService.transferSelected();
    }
    
    arrowEnable(side: string): boolean {
        const selectedPlayer = this.playerService.getSelectedPlayer();
        const isSelectedPlayerPicked = !selectedPlayer ? false : selectedPlayer.isPicked;
        
        if (!selectedPlayer) {
            return true;
        } else if (isSelectedPlayerPicked && side === PlayerSide.Right) {
            return true;
        } else if (!isSelectedPlayerPicked && side === PlayerSide.Left) {
            return true;
        }
        
        return false;
    }
}
