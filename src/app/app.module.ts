import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {PlayerListComponent} from './player-list/player-list.component';
import {PlayerService} from './services/PlayerService';
import {PlayerComponent} from './player/player.component';
import {PlayerPickerComponent} from './player-picker/player-picker.component';
import {PickedPlayerPipe} from './picked-player.pipe';
import {PlayerFormComponent} from './player-form/player-form.component';


@NgModule({
    declarations: [
        AppComponent,
        PlayerListComponent,
        PlayerComponent,
        PlayerPickerComponent,
        PickedPlayerPipe,
        PlayerFormComponent
    ],
    imports: [
        BrowserModule,
        FormsModule
    ],
    providers: [PlayerService],
    bootstrap: [AppComponent]
})
export class AppModule {
}
