export class Player {
    id: string;
    firstName: string;
    lastName: string;
    shirtNumber: number;
    position: string;
    isPicked: boolean;
    
    constructor(id: string, firstName: string, lastName = '', shirtNumber: number, position = '', isPicked: false) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.shirtNumber = shirtNumber;
        this.position = position;
        this.isPicked = isPicked;
    }
}
