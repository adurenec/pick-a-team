import {Component, OnDestroy, OnInit} from '@angular/core';
import {PlayerService} from './services/PlayerService';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    constructor(private playerService: PlayerService) {
    }
    
    onBtnClick() {
        this.playerService.setFormVisibility(true);
    }
}
