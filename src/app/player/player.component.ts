import {Component, Input} from '@angular/core';
import {Player} from '../models/Player';
import {PlayerService} from '../services/PlayerService';

@Component({
    selector: 'app-player',
    templateUrl: './player.component.html',
    styleUrls: ['./player.component.scss']
})
export class PlayerComponent {
    @Input() player: Player;
    
    constructor(private playerService: PlayerService) {
    }
    
    playerTileClicked() {
        this.playerService.selectPlayer.next(this.player.id);
    }
    
    onRemoveClick() {
        this.playerService.deletePlayer(this.player.id);
    }
    
    onEditClick(evt) {
        evt.stopPropagation();
        this.playerService.formSubject.next(this.player);
    }
    
}
